import Button from "../components/Button";
import "./Recommended.css";

const Recommended = ({ handleClick }) => {
  return (
    <>
      <div>
        <h2 className="recommended-title">Recommended Blazers</h2>
        <div className="recommended-flex">
          <Button onClickHandler={handleClick} value="" title="All Products" />
          <Button onClickHandler={handleClick} value="van" title="Van Heusen" />
          <Button onClickHandler={handleClick} value="raymond" title="Raymond" />
          <Button onClickHandler={handleClick} value="peter" title="Peter England" />
          <Button onClickHandler={handleClick} value="louis" title="Louis Philippe" />
        </div>
      </div>
    </>
  );
};

export default Recommended;
