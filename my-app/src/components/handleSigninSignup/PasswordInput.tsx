import { useState } from "react";
import "./PasswordInput.css";
import { EyeOutlined, EyeInvisibleOutlined } from "@ant-design/icons";

function PasswordInput() {
  const [password, setPassword] = useState("");
  const [visible, setVisible] = useState(true);

  return (
    <div className="password-input">
      <div className="container">
        <input
          value={password}
          type={visible ? "text" : "password"}
          className="password-box"
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
        />
        <div className="icon" onClick={() => setVisible(!visible)}>
          {visible ? (
            <EyeOutlined className="black-icon" />
          ) : (
            <EyeInvisibleOutlined className="black-icon" />
          )}
        </div>
      </div>
    </div>
  );
}

export default PasswordInput;
