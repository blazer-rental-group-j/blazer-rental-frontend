import "./Category.css";
import Input from "../../components/Input";

function Category({ handleChange }) {
  return (
    <div>
      <h2 className="sidebar-title">Category</h2>

      <div>
        <label className="sidebar-label-container">
          <input onChange={handleChange} type="radio" value="" name="test" />
          <span className="checkmark"></span>All
        </label>
        <Input
          handleChange={handleChange}
          value="tuxedo"
          title="Tuxedo Jacket"
          name="test"
        />
        <Input
          handleChange={handleChange}
          value="cape"
          title="Cape Blazer"
          name="test"
        />
        <Input
          handleChange={handleChange}
          value="lace"
          title="Lace Blazer"
          name="test"
        />
        <Input
          handleChange={handleChange}
          value="linen"
          title="Linen blazer"
          name="test"
        />
      </div>
    </div>
  );
}

export default Category;
