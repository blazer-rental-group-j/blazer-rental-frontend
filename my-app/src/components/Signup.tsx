import "./Signup.css";

function Signup() {
  return (
    <div className="app-signup">
      <div className="logo">blazer.io</div>
      <div className="login-form">
        <h1>Sign Up</h1>
        <div className="text-input">
          <input
            type="text"
            className="form-control"
            placeholder="Username"
            aria-describedby="basic-addon1"
          />
        </div>
        <div className="text-input ">
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            aria-describedby="basic-addon1"
          />
        </div>
        <div className="text-input ">
          <input
            type="password"
            className="form-control"
            placeholder="Re-enter Password"
            aria-describedby="basic-addon1"
          />
        </div>
        <div className="d-grid gap-1">
          <button className="btn btn-dark mt-3" type="button">
            Sign up
          </button>
        </div>
        <hr className="hr" />
        <div className="d-grid gap-1">
          <button className="btn btn-outline-dark" type="button">
            Sign in
          </button>
        </div>
      </div>
    </div>
  );
}

export default Signup;
